package players_test

import (
	"bitbucket.org/c-j-j/tictactoe-go/core/board"
	"bitbucket.org/c-j-j/tictactoe-go/core/players"
	"bitbucket.org/c-j-j/tictactoe-go/io"
	"github.com/stretchr/testify/suite"
	"testing"
)

type HumanPlayerSuite struct {
	suite.Suite
	player     players.HumanPlayer
	spyDisplay io.SpyDisplay
}

func (suite *HumanPlayerSuite) SetupTest() {
	suite.player = players.NewHumanPlayer(&suite.spyDisplay)
}

func (suite *HumanPlayerSuite) TestGetsMoveFromDisplay() {
	suite.spyDisplay.SetNextUserMove(2)
	suite.Equal(2, suite.player.GetMove(board.NewBoard()))
}

func TestHumanPlayerSuite(t *testing.T) {
	suite.Run(t, new(HumanPlayerSuite))
}
