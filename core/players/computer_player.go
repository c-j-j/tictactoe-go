package players

import (
	"bitbucket.org/c-j-j/tictactoe-go/core/board"
)

type ComputerPlayer struct {
}

type Outcome struct {
	Score    int
	Position int
}

const infinity = 100000
const ComputerLostScore = -10
const ComputerDrawnScore = 0
const invalidMove = -1

func NewComputerPlayer() ComputerPlayer {
	return ComputerPlayer{}
}

func (player ComputerPlayer) GetMove(board board.Board) int {
	return Negamax(board).Position
}

func Negamax(board board.Board) Outcome {
	return negamaxWithAlphaBetaPruning(board, -infinity, infinity)
}

func negamaxWithAlphaBetaPruning(board board.Board, alpha int, beta int) Outcome {
	if board.HasFinished() {
		return calculateScore(board)
	}

	bestOutcome := worstOutcome()
	for _, nextBoard := range board.NextPossibleBoards() {
		potentialOutcome := negamaxWithAlphaBetaPruning(nextBoard, -beta, -alpha).negate()
		if potentialOutcome.Score > bestOutcome.Score {
			bestOutcome = Outcome{potentialOutcome.Score, board.FindDifferences(nextBoard)[0]}
		}

		alpha = max(alpha, bestOutcome.Score)
		if alpha >= beta {
			break
		}
	}
	return bestOutcome
}

func max(a int, b int) int {
	if a >= b {
		return a
	} else {
		return b
	}
}

func worstOutcome() Outcome {
	return Outcome{-infinity, invalidMove}
}

func (outcome Outcome) negate() Outcome {
	return Outcome{-outcome.Score, outcome.Position}
}

func calculateScore(board board.Board) Outcome {
	if board.HasBeenWon() {
		return Outcome{ComputerLostScore, invalidMove}
	} else {
		return Outcome{ComputerDrawnScore, invalidMove}
	}
}
