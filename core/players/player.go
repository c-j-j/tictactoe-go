package players

import (
	"bitbucket.org/c-j-j/tictactoe-go/core/board"
)

type Player interface {
	GetMove(board board.Board) int
}
