package players

import (
	"bitbucket.org/c-j-j/tictactoe-go/core/board"
	"bitbucket.org/c-j-j/tictactoe-go/io"
)

type HumanPlayer struct {
	display io.Display
}

func NewHumanPlayer(display io.Display) HumanPlayer {
	return HumanPlayer{display}
}

func (player HumanPlayer) GetMove(_ board.Board) int {
	return player.display.GetMoveFromUser()
}
