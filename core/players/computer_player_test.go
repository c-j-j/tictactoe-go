package players_test

import (
	"bitbucket.org/c-j-j/tictactoe-go/core/board"
	"bitbucket.org/c-j-j/tictactoe-go/core/game"
	"bitbucket.org/c-j-j/tictactoe-go/core/players"
	"bitbucket.org/c-j-j/tictactoe-go/io"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestSuite struct {
	suite.Suite
	player players.ComputerPlayer
	board  board.Board
}

func (suite *TestSuite) SetupTest() {
	suite.player = players.NewComputerPlayer()
	suite.board = board.NewBoard()
}

func (suite *TestSuite) TestNegamaxScoreWhenGameHasEndedInWin() {
	suite.board.AddMoves(0, 3, 1, 4, 2)
	result := players.Negamax(suite.board)
	suite.Equal(players.ComputerLostScore, result.Score)
}

func (suite *TestSuite) TestNegamaxScoreWhenGameHasEndedInDraw() {
	suite.board.AddMoves(0, 2, 1, 3, 5, 4, 6, 7, 8)
	result := players.Negamax(suite.board)
	suite.Equal(players.ComputerDrawnScore, result.Score)
}

func (suite *TestSuite) TestNegamaxNegatesScore() {
	suite.board.AddMoves(0, 3, 1, 4, 5, 6, 7, 8)
	result := players.Negamax(suite.board)
	suite.Equal(-players.ComputerLostScore, result.Score)
}

func (suite *TestSuite) TestNegamaxPicksHighestScore() {
	suite.board.AddMoves(0, 3, 1, 4, 5, 6, 7)
	result := players.Negamax(suite.board)
	suite.Equal(-players.ComputerLostScore, result.Score)
}

func (suite *TestSuite) TestComputerPlayerPicksCornerFirst() {
	move := suite.player.GetMove(suite.board)
	suite.Contains([]int{0, 2, 6, 8}, move)
}

func (suite *TestSuite) TestComputerPlayerBlocksOpponent() {
	suite.board.AddMoves(0, 8, 2)
	move := suite.player.GetMove(suite.board)
	suite.Equal(1, move)
}

func (suite *TestSuite) TestComputerPlayerForks() {
	suite.board.AddMoves(0, 1, 4, 8)
	move := suite.player.GetMove(suite.board)
	suite.Equal(3, move)
}

func (suite *TestSuite) TestComputerPlayerForksWhenOpponentInOppositeCorner() {
	suite.board.AddMoves(0, 1, 2, 8)
	move := suite.player.GetMove(suite.board)
	suite.Equal(6, move)
}

func (suite *TestSuite) TestComputerVsComputerEndsInDraw() {
	display := io.NewSpyDisplay()
	game.NewGame(board.NewBoard(), &display, players.NewComputerPlayer(), players.NewComputerPlayer()).Play()
	suite.Equal(true, display.DrawOutcomePrinted)
}

func TestTestSuite(t *testing.T) {
	suite.Run(t, new(TestSuite))
}
