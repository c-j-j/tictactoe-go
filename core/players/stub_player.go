package players

import "bitbucket.org/c-j-j/tictactoe-go/core/board"

type StubPlayer struct {
	preparedMoves []int
}

func (player *StubPlayer) GetMove(_ board.Board) int {
	nextMove := player.preparedMoves[0]
	player.preparedMoves = player.preparedMoves[1:]
	return nextMove
}

func (player *StubPlayer) PrepareMoves(nextMovePositions ...int) {
	player.preparedMoves = append(player.preparedMoves, nextMovePositions...)
}
