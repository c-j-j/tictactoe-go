package board

type Board struct {
	marks []Mark
}

type Mark int

type line []Mark

const (
	EMPTY Mark = iota
	X     Mark = iota
	O     Mark = iota
)

const rowSize = 3

func NewBoard() Board {
	return Board{make([]Mark, rowSize*rowSize)}
}

func (board Board) Size() int {
	return len(board.marks)
}

func (board Board) Rows() []line {
	var rows []line
	for rowNumber := 0; rowNumber < rowSize; rowNumber++ {
		rows = append(rows, board.row(rowNumber))
	}
	return rows
}

func (line line) Length() int {
	return len(line)
}

func (board *Board) AddMove(position int) {
	board.marks[position] = board.CurrentPlayerMark()
}

func (board *Board) AddMoves(positions ...int) {
	for _, position := range positions {
		board.AddMove(position)
	}
}

func (board Board) CurrentPlayerMark() Mark {
	if board.IsPlayerOneNext() {
		return X
	} else {
		return O
	}
}

func (board Board) MarkAtPosition(position int) Mark {
	return board.marks[position]
}

func (board Board) HasFinished() bool {
	return board.HasBeenDrawn() || board.HasBeenWon()
}

func (board Board) HasBeenWon() bool {
	return board.findWinningLine() != nil
}

func (board Board) WinningMark() Mark {
	line := board.findWinningLine()
	if line != nil {
		return line[0]
	} else {
		return EMPTY
	}
}

func (board Board) HasBeenDrawn() bool {
	return !board.HasBeenWon() && board.emptyMarkCount() == 0
}

func (board Board) IsMoveValid(move int) bool {
	return move >= 0 && move < board.Size() && board.isPositionEmpty(move)
}

func (board Board) NextPossibleBoards() []Board {
	var boards []Board
	for i := 0; i < board.Size(); i++ {
		if board.MarkAtPosition(i) == EMPTY {
			boardCopy := board.copy()
			boardCopy.AddMove(i)
			boards = append(boards, boardCopy)
		}
	}
	return boards
}

func (board Board) FindDifferences(anotherBoard Board) []int {
	var positions []int
	for i := range board.marks {
		if board.marks[i] != anotherBoard.marks[i] {
			positions = append(positions, i)
		}
	}
	return positions
}

func (board Board) IsPlayerOneNext() bool {
	return board.emptyMarkCount()%2 != 0
}

func (mark Mark) IsEmpty() bool {
	return mark == EMPTY
}

func (board Board) lines() []line {
	var lines []line
	for _, linePositions := range positionsOfAllLines() {
		lines = append(lines, board.createLine(linePositions))
	}
	return lines
}

func positionsOfAllLines() [][]int {
	return [][]int{
		{0, 1, 2},
		{3, 4, 5},
		{6, 7, 8},
		{0, 3, 6},
		{1, 4, 7},
		{2, 5, 8},
		{0, 4, 8},
		{2, 4, 6},
	}
}

func (board Board) emptyMarkCount() int {
	count := 0
	for _, mark := range board.marks {
		if mark.IsEmpty() {
			count++
		}
	}
	return count
}

func (board Board) row(rowNumber int) line {
	rowIndex := rowNumber * rowSize
	return board.marks[rowIndex : rowIndex+rowSize]
}

func (board Board) createLine(positions []int) line {
	var line line
	for _, position := range positions {
		line = append(line, board.marks[position])
	}
	return line
}

func (line line) containsEmptyMark() bool {
	for _, mark := range line {
		if mark.IsEmpty() {
			return true
		}
	}
	return false
}

func (board Board) copy() Board {
	copyOfMarks := make([]Mark, board.Size())
	copy(copyOfMarks, board.marks)
	return Board{copyOfMarks}
}

func (line line) allMarksEqual() bool {
	for _, mark := range line {
		if line[0] != mark {
			return false
		}
	}
	return true
}

func (board Board) isPositionEmpty(move int) bool {
	return board.marks[move] == EMPTY
}

func (board Board) findWinningLine() line {
	for _, line := range board.lines() {
		if line.hasBeenWon() {
			return line
		}
	}
	return nil
}

func (l line) hasBeenWon() bool {
	return l.allMarksEqual() && !l.containsEmptyMark()
}
