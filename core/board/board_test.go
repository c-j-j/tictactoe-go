package board_test

import (
	"bitbucket.org/c-j-j/tictactoe-go/core/board"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestSuite struct {
	suite.Suite
	board board.Board
}

func (suite *TestSuite) SetupTest() {
	suite.board = board.NewBoard()
}

func (suite *TestSuite) TestCreatesNewBoard() {
	testBoard := board.NewBoard()
	suite.Equal(9, testBoard.Size())
}

func (suite *TestSuite) TestAddsFirstMoveToBoardInPositionO() {
	suite.board.AddMove(0)
	suite.Equal(suite.board.MarkAtPosition(0), board.X)
}

func (suite *TestSuite) TestAddsFirstMoveToBoardInPosition1() {
	suite.board.AddMove(1)
	suite.Equal(suite.board.MarkAtPosition(1), board.X)
}

func (suite *TestSuite) TestAddsSecondMove() {
	suite.board.AddMove(0)
	suite.board.AddMove(1)
	suite.Equal(suite.board.MarkAtPosition(0), board.X)
	suite.Equal(suite.board.MarkAtPosition(1), board.O)
}

func (suite *TestSuite) TestWhenThereIsNoWinner() {
	suite.Equal(false, suite.board.HasBeenWon())
}

func (suite *TestSuite) TestWinnerOccupiesTopRow() {
	suite.board.AddMoves(0, 6, 1, 7, 2)
	suite.Equal(true, suite.board.HasBeenWon())
}

func (suite *TestSuite) TestWinnerOccupiesMiddleRow() {
	suite.board.AddMoves(3, 6, 4, 7, 5)
	suite.Equal(true, suite.board.HasBeenWon())
}

func (suite *TestSuite) TestWinnerOccupiesBottomRow() {
	suite.board.AddMoves(6, 0, 7, 1, 8)
	suite.Equal(true, suite.board.HasBeenWon())
}

func (suite *TestSuite) TestWinnerOccupiesLeftColumn() {
	suite.board.AddMoves(0, 1, 3, 2, 6)
	suite.Equal(true, suite.board.HasBeenWon())
}

func (suite *TestSuite) TestWinnerOccupiesMiddleColumn() {
	suite.board.AddMoves(1, 1, 4, 2, 7)
	suite.Equal(true, suite.board.HasBeenWon())
}

func (suite *TestSuite) TestWinnerOccupiesRightColumn() {
	suite.board.AddMoves(2, 1, 5, 3, 8)
	suite.Equal(true, suite.board.HasBeenWon())
}

func (suite *TestSuite) TestWinnerOccupiesDiagonalStartingTopLeft() {
	suite.board.AddMoves(0, 1, 4, 3, 8)
	suite.Equal(true, suite.board.HasBeenWon())
}

func (suite *TestSuite) TestWinnerOccupiesDiagonalStartingTopRight() {
	suite.board.AddMoves(2, 1, 4, 3, 6)
	suite.Equal(true, suite.board.HasBeenWon())
}

func (suite *TestSuite) TestFullBoardHasEndedInDraw() {
	suite.board.AddMoves(0, 2, 1, 3, 5, 4, 6, 7, 8)
	suite.Equal(true, suite.board.HasBeenDrawn())
}

func (suite *TestSuite) TestFullBoardHasNotEndedInDraw() {
	suite.board.AddMoves(0, 1, 4, 2, 5, 3, 7, 6, 8)
	suite.Equal(false, suite.board.HasBeenDrawn())
}

func (suite *TestSuite) TestBoardHasFinishedDueToDraw() {
	suite.Equal(false, suite.board.HasFinished())
	suite.board.AddMoves(0, 2, 1, 3, 5, 4, 6, 7, 8)
	suite.Equal(true, suite.board.HasFinished())
}

func (suite *TestSuite) TestBoardHasFinishedDueToWin() {
	suite.Equal(false, suite.board.HasFinished())
	suite.board.AddMoves(2, 1, 4, 3, 6)
	suite.Equal(true, suite.board.HasFinished())
}

func (suite *TestSuite) TestNextPossibleBoards() {
	initialBoard := board.NewBoard()
	nextPossibleBoards := initialBoard.NextPossibleBoards()
	suite.Equal(board.NewBoard(), initialBoard, "initial board modified")
	for i := 0; i < initialBoard.Size(); i++ {
		testBoard := board.NewBoard()
		testBoard.AddMove(i)
		suite.Equal(testBoard, nextPossibleBoards[i])
	}
}

func (suite *TestSuite) TestMoveIsInvalidIfPositionNotEmpty() {
	suite.Equal(true, suite.board.IsMoveValid(1))
	suite.board.AddMove(1)
	suite.Equal(false, suite.board.IsMoveValid(1))
}

func (suite *TestSuite) TestMoveIsInvalidIfExceedsBoardBoundaries() {
	suite.Equal(false, suite.board.IsMoveValid(-1))
	suite.Equal(false, suite.board.IsMoveValid(suite.board.Size()))
}

func (suite *TestSuite) TestCurrentPlayerMarkPrinted() {
	suite.Equal(board.X, suite.board.CurrentPlayerMark())
}

func (suite *TestSuite) TestDifferenceInBoards() {
	alteredBoard := board.NewBoard()
	alteredBoard.AddMove(4)
	suite.Equal(4, suite.board.FindDifferences(alteredBoard)[0])
}

func (suite *TestSuite) TestBoardHasRows() {
	suite.board.AddMoves(0, 3, 6)
	rows := suite.board.Rows()
	suite.Equal(3, rows[0].Length())
	suite.Contains(rows[0], board.X)
	suite.Contains(rows[1], board.O)
	suite.Contains(rows[2], board.X)
}

func (suite *TestSuite) TestEmptyBoardHasNoWinningMark() {
	suite.Equal(board.EMPTY, suite.board.WinningMark())
}

func (suite *TestSuite) TestBoardHasWinningMark() {
	suite.board.AddMoves(0, 3, 1, 4, 2)
	suite.Equal(board.X, suite.board.WinningMark())
}

func TestTestSuite(t *testing.T) {
	suite.Run(t, new(TestSuite))
}
