package setup

import (
	"bitbucket.org/c-j-j/tictactoe-go/core/board"
	"bitbucket.org/c-j-j/tictactoe-go/core/game"
	"bitbucket.org/c-j-j/tictactoe-go/core/players"
	"bitbucket.org/c-j-j/tictactoe-go/io"
	"fmt"
)

type GameSetup struct {
	display     io.Display
	gameOptions map[string]createGame
}

const HVHOption = "Human Vs Human"
const HVCOption = "Human Vs Computer"
const CVHOption = "Computer Vs Human"
const CVCOption = "Computer Vs Computer"

type createGame func(io.Display) game.Game

func NewSetup(display io.Display) GameSetup {
	return GameSetup{display, gameOptions()}
}

func (gameSetup GameSetup) CreateNewGame() game.Game {
	chosenGameOption := gameSetup.display.ChooseGameOption(descriptions(gameSetup.gameOptions))
	createGame := gameSetup.gameOptions[chosenGameOption]
	if createGame == nil {
		panic(fmt.Sprintln("Chosen Game Option is a not a valid choice: ", chosenGameOption))
	}
	return createGame(gameSetup.display)
}

func gameOptions() map[string]createGame {
	return map[string]createGame{
		HVHOption: createHVHGame,
		HVCOption: createHVCGame,
		CVHOption: createCVHGame,
		CVCOption: createCVCGame,
	}
}

func createHVHGame(display io.Display) game.Game {
	return game.NewGame(board.NewBoard(), display, players.NewHumanPlayer(display), players.NewHumanPlayer(display))
}

func createHVCGame(display io.Display) game.Game {
	return game.NewGame(board.NewBoard(), display, players.NewHumanPlayer(display), players.NewComputerPlayer())
}

func createCVHGame(display io.Display) game.Game {
	return game.NewGame(board.NewBoard(), display, players.NewComputerPlayer(), players.NewHumanPlayer(display))
}

func createCVCGame(display io.Display) game.Game {
	return game.NewGame(board.NewBoard(), display, players.NewComputerPlayer(), players.NewComputerPlayer())
}

func descriptions(gameOptions map[string]createGame) []string {
	var descriptions []string
	for description := range gameOptions {
		descriptions = append(descriptions, description)
	}
	return descriptions
}
