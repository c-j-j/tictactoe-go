package setup

import (
	"bitbucket.org/c-j-j/tictactoe-go/core/game"
	"bitbucket.org/c-j-j/tictactoe-go/core/players"
	"bitbucket.org/c-j-j/tictactoe-go/io"
	"github.com/stretchr/testify/suite"
	"reflect"
	"testing"
)

type GameSetupTestSuite struct {
	suite.Suite
}

func (suite *GameSetupTestSuite) TestBuildsHVHGame() {
	newGame := suite.createGameWithOption(HVHOption)
	suite.Equal(reflect.TypeOf(newGame.PlayerOne), reflect.TypeOf(players.NewHumanPlayer(nil)))
	suite.Equal(reflect.TypeOf(newGame.PlayerTwo), reflect.TypeOf(players.NewHumanPlayer(nil)))
}

func (suite *GameSetupTestSuite) TestBuildsHVCGame() {
	newGame := suite.createGameWithOption(HVCOption)
	suite.Equal(reflect.TypeOf(newGame.PlayerOne), reflect.TypeOf(players.NewHumanPlayer(nil)))
	suite.Equal(reflect.TypeOf(newGame.PlayerTwo), reflect.TypeOf(players.NewComputerPlayer()))
}

func (suite *GameSetupTestSuite) TestBuildsCVHGame() {
	newGame := suite.createGameWithOption(CVHOption)
	suite.Equal(reflect.TypeOf(newGame.PlayerOne), reflect.TypeOf(players.NewComputerPlayer()))
	suite.Equal(reflect.TypeOf(newGame.PlayerTwo), reflect.TypeOf(players.NewHumanPlayer(nil)))
}

func (suite *GameSetupTestSuite) TestBuildsCVCGame() {
	newGame := suite.createGameWithOption(CVCOption)
	suite.Equal(reflect.TypeOf(newGame.PlayerOne), reflect.TypeOf(players.NewComputerPlayer()))
	suite.Equal(reflect.TypeOf(newGame.PlayerTwo), reflect.TypeOf(players.NewComputerPlayer()))
}

func (suite *GameSetupTestSuite) createGameWithOption(option string) game.Game {
	display := io.NewSpyDisplay()
	gameSetup := NewSetup(&display)
	display.SetChosenGame(option)
	return gameSetup.CreateNewGame()
}

func TestGameSetupTestSuite(t *testing.T) {
	suite.Run(t, new(GameSetupTestSuite))
}
