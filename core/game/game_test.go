package game_test

import (
	"bitbucket.org/c-j-j/tictactoe-go/core/board"
	"bitbucket.org/c-j-j/tictactoe-go/core/game"
	"bitbucket.org/c-j-j/tictactoe-go/core/players"
	"bitbucket.org/c-j-j/tictactoe-go/io"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestSuite struct {
	suite.Suite
	game       game.Game
	board      board.Board
	SpyDisplay io.SpyDisplay
	playerOne  players.StubPlayer
	playerTwo  players.StubPlayer
}

func (suite *TestSuite) SetupTest() {
	suite.board = board.NewBoard()
	suite.SpyDisplay = *new(io.SpyDisplay)
	suite.playerOne = *new(players.StubPlayer)
	suite.playerTwo = *new(players.StubPlayer)
	suite.game = game.NewGame(suite.board, &suite.SpyDisplay, &suite.playerOne, &suite.playerTwo)
}

func (suite *TestSuite) TestDisplaysNewBoard() {
	suite.board.AddMoves(0, 4, 1, 5, 2)
	suite.game.Play()
	suite.Equal(suite.board, suite.SpyDisplay.BoardPrinted)
}

func (suite *TestSuite) TestPlayerOneMakesMove() {
	suite.board.AddMoves(0, 3, 1, 4)
	suite.playerOne.PrepareMoves(2)
	suite.Equal(board.EMPTY, suite.board.MarkAtPosition(2))
	suite.game.Play()
	suite.NotEqual(board.EMPTY, suite.board.MarkAtPosition(2))
}

func (suite *TestSuite) TestBothPlayersMakeMoves() {
	suite.board.AddMoves(0, 6, 4, 7, 5)
	suite.playerTwo.PrepareMoves(8)
	suite.Equal(board.EMPTY, suite.board.MarkAtPosition(8))
	suite.game.Play()
	suite.NotEqual(board.EMPTY, suite.board.MarkAtPosition(8))
}

func (suite *TestSuite) TestUserIsPromptedWhenInvalidMoveMade() {
	suite.board.AddMoves(0, 3, 1, 4)
	suite.playerOne.PrepareMoves(4, 2)
	suite.game.Play()
	suite.Equal(true, suite.SpyDisplay.InvalidMoveMessagePrinted)
}

func (suite *TestSuite) TestNextPlayersTurnIsPrinted() {
	suite.board.AddMoves(0, 3, 1, 4)
	suite.playerOne.PrepareMoves(2)
	currentPlayerMark := suite.board.CurrentPlayerMark()
	suite.game.Play()
	suite.Equal(currentPlayerMark, suite.SpyDisplay.CurrentPlayerTurnPrinted)
}

func (suite *TestSuite) TestWinningOutcomeIsPrinted() {
	suite.board.AddMoves(0, 3, 1, 4, 2)
	suite.game.Play()
	suite.Equal(suite.board.WinningMark(), suite.SpyDisplay.WinnerMessagePrintedWithMark)
}

func (suite *TestSuite) TestDrawnOutcomeIsPrinted() {
	suite.board.AddMoves(0, 2, 1, 3, 5, 4, 6, 7, 8)
	suite.game.Play()
	suite.Equal(true, suite.SpyDisplay.DrawOutcomePrinted)
}

func TestTestSuite(t *testing.T) {
	suite.Run(t, new(TestSuite))
}
