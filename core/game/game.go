package game

import (
	"bitbucket.org/c-j-j/tictactoe-go/core/board"
	"bitbucket.org/c-j-j/tictactoe-go/core/players"
	"bitbucket.org/c-j-j/tictactoe-go/io"
)

type Game struct {
	board     board.Board
	display   io.Display
	PlayerOne players.Player
	PlayerTwo players.Player
}

func NewGame(board board.Board, display io.Display, PlayerOne players.Player, PlayerTwo players.Player) Game {
	return Game{board, display, PlayerOne, PlayerTwo}
}

func (game Game) Play() {
	game.displayBoard()
	for game.isInPlay() {
		game.printCurrentPlayerTurn()
		game.playTurn()
		game.displayBoard()
	}
	game.displayOutput()
}

func (game Game) playTurn() {
	move := game.getMoveFromCurrentPlayer()
	if game.isMoveValid(move) {
		game.addMoveToBoard(move)
	} else {
		game.displayInvalidMoveMessage()
	}
}

func (game Game) printCurrentPlayerTurn() {
	game.display.PrintCurrentPlayerTurn(game.board.CurrentPlayerMark())
}

func (game Game) addMoveToBoard(move int) {
	game.board.AddMove(move)
}

func (game Game) displayOutput() {
	if game.board.HasBeenWon() {
		game.display.PrintWinOutcome(game.board.WinningMark())
	} else {
		game.display.PrintDrawOutcome()
	}
}

func (game Game) displayInvalidMoveMessage() {
	game.display.PrintInvalidMoveMessage()
}

func (game Game) isMoveValid(move int) bool {
	return game.board.IsMoveValid(move)
}

func (game Game) getMoveFromCurrentPlayer() int {
	if game.board.IsPlayerOneNext() {
		return game.PlayerOne.GetMove(game.board)
	} else {
		return game.PlayerTwo.GetMove(game.board)
	}
}

func (game Game) isInPlay() bool {
	return !game.board.HasFinished()
}

func (game Game) displayBoard() {
	game.display.PrintBoard(game.board)
}
