package io

import (
	"bitbucket.org/c-j-j/tictactoe-go/core/board"
)

type SpyDisplay struct {
	BoardPrinted                 board.Board
	usersNextMove                int
	InvalidMoveMessagePrinted    bool
	CurrentPlayerTurnPrinted     board.Mark
	WinnerMessagePrintedWithMark board.Mark
	DrawOutcomePrinted           bool
	chosenGameOption             string
}

func NewSpyDisplay() SpyDisplay {
	return SpyDisplay{}
}

func (display *SpyDisplay) PrintBoard(board board.Board) {
	display.BoardPrinted = board
}

func (display *SpyDisplay) PrintInvalidMoveMessage() {
	display.InvalidMoveMessagePrinted = true
}

func (display *SpyDisplay) PrintCurrentPlayerTurn(mark board.Mark) {
	display.CurrentPlayerTurnPrinted = mark
}

func (display *SpyDisplay) SetChosenGame(gameOption string) {
	display.chosenGameOption = gameOption
}

func (display *SpyDisplay) ChooseGameOption(gameOptions []string) string {
	return display.chosenGameOption
}

func (display *SpyDisplay) PlayAgain() bool {
	return true
}

func (display *SpyDisplay) PrintWinOutcome(winningMark board.Mark) {
	display.WinnerMessagePrintedWithMark = winningMark
}

func (display *SpyDisplay) PrintDrawOutcome() {
	display.DrawOutcomePrinted = true
}

func (display *SpyDisplay) GetMoveFromUser() int {
	return display.usersNextMove
}

func (display *SpyDisplay) SetNextUserMove(nextMove int) {
	display.usersNextMove = nextMove
}
