package io

import (
	"bitbucket.org/c-j-j/tictactoe-go/core/board"
)

type Display interface {
	PrintBoard(board board.Board)
	GetMoveFromUser() int
	PrintInvalidMoveMessage()
	PrintCurrentPlayerTurn(mark board.Mark)
	PrintWinOutcome(mark board.Mark)
	PrintDrawOutcome()
	ChooseGameOption(gameDescriptions []string) string
	PlayAgain() bool
}
