package io

import (
	b "bitbucket.org/c-j-j/tictactoe-go/core/board"
	"bufio"
	"errors"
	"fmt"
	"github.com/fatih/color"
	"io"
	"os"
	"strconv"
	"strings"
)

const EnterMoveInstruction = "Please enter a move:"
const ChooseGameOptionInstruction = "Please choose a game option:"
const PlayAgainInstruction = "Would you like to play another game?"
const InvalidMoveMessage = "I'm sorry, that is not a valid move"
const CurrentPlayerMessagePrefix = "It is now "
const CurrentPlayerMessageSuffix = "'s turn..."
const DrawOutcomeMessage = "This game has ended in a draw."
const WinOutcomeMessageSuffix = " has won this game."
const InvalidChoiceMessage = "I'm sorry, that is not a valid choice."

const playAgainYesChoice = "Yes"
const playAgainNoChoice = "No"

const errorTextColour = color.FgRed
const defaultColour = color.FgWhite
const xColour = color.FgGreen
const oColour = color.FgBlue

type ConsoleDisplay struct {
	Reader bufio.Reader
	Writer io.Writer
}

const CellOffset = 1

func NewConsoleDisplay() ConsoleDisplay {
	return ConsoleDisplay{*bufio.NewReader(os.Stdin), os.Stdout}
}

func (display ConsoleDisplay) ChooseGameOption(gameOptions []string) string {
	display.printLine(ChooseGameOptionInstruction)
	return display.getValidOptionFromUser(gameOptions...)
}

func (display ConsoleDisplay) GetMoveFromUser() int {
	display.printLine(EnterMoveInstruction)
	return display.readInteger() - CellOffset
}

func (display ConsoleDisplay) PlayAgain() bool {
	display.printLine(PlayAgainInstruction)
	chosenOption := display.getValidOptionFromUser(playAgainYesChoice, playAgainNoChoice)
	return parsePlayAgainOption(chosenOption)
}

func (display ConsoleDisplay) PrintInvalidMoveMessage() {
	display.printfWithColour(InvalidMoveMessage, errorTextColour)
	display.newLine()
}

func (display ConsoleDisplay) PrintCurrentPlayerTurn(mark b.Mark) {
	display.printf(CurrentPlayerMessagePrefix)
	display.printfWithColour(cellText(mark), cellColour(mark))
	display.printf(CurrentPlayerMessageSuffix)
	display.newLine()
}

func (display ConsoleDisplay) PrintWinOutcome(mark b.Mark) {
	display.printfWithColour(cellText(mark), cellColour(mark))
	display.printf(WinOutcomeMessageSuffix)
	display.newLine()
}

func (display ConsoleDisplay) PrintDrawOutcome() {
	display.printLine(DrawOutcomeMessage)
}

func (display ConsoleDisplay) PrintBoard(board b.Board) {
	for rowNumber, row := range board.Rows() {
		display.printRowDivider(rowNumber)
		display.addLeftPadding()
		for columnNumber, cell := range row {
			display.printCell(cell, determineCellNumber(rowNumber, columnNumber, row.Length()))
			display.printColumnDivider(columnNumber, row.Length())
		}
		display.newLine()
	}
	display.newLine()
}

func (display ConsoleDisplay) printf(message string) {
	fmt.Fprintf(display.Writer, message)
}

func (display ConsoleDisplay) printLine(message string) {
	fmt.Fprintln(display.Writer, message)
}

func (display ConsoleDisplay) getValidOptionFromUser(options ...string) string {
	for {
		display.printOptions(options)
		if option, error := display.tryToGetOptionFromUser(options); error != nil {
			display.printfWithColour(InvalidChoiceMessage, errorTextColour)
			display.newLine()
		} else {
			return option
		}
	}
}

func (display *ConsoleDisplay) tryToGetOptionFromUser(options []string) (string, error) {
	chosenOption := display.readInteger()
	if (chosenOption > 0) && (chosenOption <= len(options)) {
		return options[chosenOption-1], nil
	} else {
		return "", errors.New("")
	}
}

func (display ConsoleDisplay) printOptions(options []string) {
	for index, option := range options {
		display.printLine(fmt.Sprintf("%d: %s", index+1, option))
	}
}

func (display ConsoleDisplay) printCell(mark b.Mark, cellNumber int) {
	if mark.IsEmpty() {
		fmt.Fprintf(display.Writer, fmt.Sprintf("%d", cellNumber))
	} else {
		display.printfWithColour(cellText(mark), cellColour(mark))
	}
}

func (display ConsoleDisplay) printfWithColour(message string, colour color.Attribute) {
	color.Set(colour)
	fmt.Fprintf(display.Writer, message)
	color.Set(defaultColour)
}

func cellColour(mark b.Mark) color.Attribute {
	if mark == b.X {
		return xColour
	} else if mark == b.O {
		return oColour
	} else {
		return defaultColour
	}
}

func (display ConsoleDisplay) printColumnDivider(columnNumber int, rowLength int) {
	if columnNumber < rowLength-1 {
		fmt.Fprintf(display.Writer, " | ")
	}
}

func (display *ConsoleDisplay) readInteger() int {
	for {
		position, parseError := stringToInt(display.readLine())
		if parseError == nil {
			return position
		}
	}
}

func determineCellNumber(rowNumber int, columnNumber int, rowLength int) int {
	return (rowNumber * rowLength) + columnNumber + CellOffset
}

func (display ConsoleDisplay) printRowDivider(rowNumber int) {
	if rowNumber > 0 {
		display.addLeftPadding()
		display.addLineSeperator()
	}
}

func (display ConsoleDisplay) newLine() {
	fmt.Fprintln(display.Writer, "")
}

func (display ConsoleDisplay) addLeftPadding() {
	fmt.Fprintf(display.Writer, "     ")
}

func (display ConsoleDisplay) addLineSeperator() {
	fmt.Fprintln(display.Writer, "----------")
}

func cellText(mark b.Mark) string {
	if mark == b.X {
		return "X"
	} else if mark == b.O {
		return "O"
	} else {
		return ""
	}
}

func stringToInt(s string) (int, error) {
	return strconv.Atoi(s)
}

func (display *ConsoleDisplay) readLine() string {
	rawInput, _ := display.Reader.ReadString('\n')
	return strings.TrimSpace(rawInput)
}

func parsePlayAgainOption(option string) bool {
	if option == playAgainYesChoice {
		return true
	} else {
		return false
	}
}
