package io_test

import (
	"bitbucket.org/c-j-j/tictactoe-go/core/board"
	ttt_io "bitbucket.org/c-j-j/tictactoe-go/io"
	"bufio"
	"bytes"
	"fmt"
	"github.com/stretchr/testify/suite"
	"regexp"
	"strings"
	"testing"
)

type TestSuite struct {
	suite.Suite
	display ttt_io.ConsoleDisplay
}

func (suite *TestSuite) SetupTest() {
	suite.display = ttt_io.NewConsoleDisplay()
	suite.prepareUserInput("-1")
}

func (suite *TestSuite) TestGetsUserMove() {
	suite.prepareUserInput("2")
	suite.Equal(1, suite.display.GetMoveFromUser())
}

func (suite *TestSuite) TestOnlyAcceptsIntegerInput() {
	suite.prepareUserInput("a\n2")
	suite.Equal(1, suite.display.GetMoveFromUser())
}

func (suite *TestSuite) TestPrintsInstructionsToGetUserMove() {
	buffer := suite.spyOnOutputBuffer()
	suite.display.GetMoveFromUser()
	suite.Contains(buffer.String(), ttt_io.EnterMoveInstruction)
}

func (suite *TestSuite) TestPrintsInvalidMoveMessage() {
	buffer := suite.spyOnOutputBuffer()
	suite.display.PrintInvalidMoveMessage()
	suite.Contains(buffer.String(), ttt_io.InvalidMoveMessage)
}

func (suite *TestSuite) TestPrintsCurrentPlayer() {
	buffer := suite.spyOnOutputBuffer()
	suite.display.PrintCurrentPlayerTurn(board.X)
	suite.Contains(buffer.String(), ttt_io.CurrentPlayerMessagePrefix)
	suite.Contains(buffer.String(), ttt_io.CurrentPlayerMessageSuffix)
	suite.Contains(buffer.String(), "X")
}

func (suite *TestSuite) TestPrintsDrawOutcome() {
	buffer := suite.spyOnOutputBuffer()
	suite.display.PrintDrawOutcome()
	suite.Contains(buffer.String(), ttt_io.DrawOutcomeMessage)
}

func (suite *TestSuite) TestPrintsWinOutcome() {
	buffer := suite.spyOnOutputBuffer()
	suite.display.PrintWinOutcome(board.X)
	suite.Contains(buffer.String(), ttt_io.WinOutcomeMessageSuffix)
	suite.Contains(buffer.String(), "X")
}

func (suite *TestSuite) TestPrintsEmptyBoardWithNumbers() {
	buffer := suite.spyOnOutputBuffer()
	board := board.NewBoard()
	suite.display.PrintBoard(board)

	suite.Equal(true, regexp.MustCompile(`(?s)1.*2.*3.*4.*5.*6.*7.*8.*9`).MatchString(buffer.String()))
}

func (suite *TestSuite) TestPrintsFullBoard() {
	buffer := suite.spyOnOutputBuffer()
	board := board.NewBoard()
	board.AddMoves(0, 1, 2, 3, 4, 5, 6, 7, 8)
	suite.display.PrintBoard(board)

	suite.Equal(true, regexp.MustCompile(`(?s)X.*O.*X.*O.*X.*O.*X.*O.*X`).MatchString(buffer.String()))
}

func (suite *TestSuite) TestUserChoosesGameOption() {
	gameOptions := [...]string{"GameOne", "GameTwo", "GameThree"}
	suite.prepareUserInput("2")
	chosenGameOption := suite.display.ChooseGameOption(gameOptions[:])
	suite.Equal("GameTwo", chosenGameOption)
}

func (suite *TestSuite) TestUserCanOnlyChooseValidGameOption() {
	gameOptions := [...]string{"GameOne", "GameTwo", "GameThree"}
	buffer := suite.spyOnOutputBuffer()
	suite.prepareUserInput("0\n4\n1")
	chosenGameOption := suite.display.ChooseGameOption(gameOptions[:])
	suite.Equal("GameOne", chosenGameOption)
	suite.Contains(buffer.String(), ttt_io.InvalidChoiceMessage)
}

func (suite *TestSuite) TestGameOptionsShownToUser() {
	buffer := suite.spyOnOutputBuffer()
	gameOptions := [...]string{"GameOne", "GameTwo", "GameThree"}
	suite.prepareUserInput("2")
	suite.display.ChooseGameOption(gameOptions[:])
	suite.Contains(buffer.String(), ttt_io.ChooseGameOptionInstruction)
	suite.Contains(buffer.String(), "GameOne")
	suite.Contains(buffer.String(), "GameTwo")
	suite.Contains(buffer.String(), "GameThree")
}

func (suite *TestSuite) TestUserSelectsToPlayAgain() {
	suite.prepareUserInput("1")
	playAgain := suite.display.PlayAgain()
	suite.Equal(true, playAgain)
}

func (suite *TestSuite) TestUserSelectsNotToPlayAgain() {
	suite.prepareUserInput("2")
	playAgain := suite.display.PlayAgain()
	suite.Equal(false, playAgain)
}

func (suite *TestSuite) TestUserCanOnlySelectValidPlayAgainOption() {
	buffer := suite.spyOnOutputBuffer()
	suite.prepareUserInput("0\n1")
	suite.display.PlayAgain()
	suite.Contains(buffer.String(), ttt_io.PlayAgainInstruction)
	suite.Contains(buffer.String(), ttt_io.InvalidChoiceMessage)
}

func (suite *TestSuite) spyOnOutputBuffer() *bytes.Buffer {
	buf := &bytes.Buffer{}
	suite.display.Writer = buf
	return buf
}

func (suite *TestSuite) prepareUserInput(userInput string) {
	reader := strings.NewReader(fmt.Sprint(userInput, "\n"))
	bufReader := bufio.NewReader(reader)
	suite.display.Reader = *bufReader
}

func TestTestSuite(t *testing.T) {
	suite.Run(t, new(TestSuite))
}
