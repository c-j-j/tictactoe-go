# TicTacToe - Go #

This application has been tested against Go 1.4.2. It may not work with previous versions.

### Installing Go on Mac OSX ###

1. Run `brew install go`

### Setting up the Go workspace ###

1. Create a Go workspace by running `mkdir $HOME/go`

2. Create the GOPATH environment variable by running `export GOPATH=$HOME/go`

3. Add the GOPATH/bin directory to your system path by running `export PATH=$PATH:$GOPATH/bin`

### Importing the project ###

1. Run `go get bitbucket.org/c-j-j/tictactoe-go`

2. This will import the project into `$GOPATH/src`

### Importing the project's dependencies ###

1. Install [godep](https://github.com/tools/godep), a dependency management tool, by running `go get github.com/tools/godep`

2. Navigate to the project directory: `cd $GOPATH/src/bitbucket.org/c-j-j/tictactoe-go/`

3. Install the dependencies to your GOPATH by running `godep restore`

### Running the tests ###

1. Run `go test bitbucket.org/c-j-j/tictactoe-go/./...`. Add `-v` for verbose test results.

### Playing the game ###

1. Play the game by running `go run $GOPATH/src/bitbucket.org/c-j-j/tictactoe-go/main.go`
