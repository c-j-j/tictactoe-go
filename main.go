package main

import (
	"bitbucket.org/c-j-j/tictactoe-go/core/setup"
	"bitbucket.org/c-j-j/tictactoe-go/io"
)

func main() {
	display := io.NewConsoleDisplay()
	gameSetup := setup.NewSetup(display)
	for {
		gameSetup.CreateNewGame().Play()

		if !display.PlayAgain() {
			break
		}
	}
}
